// dumb tests to start going
describe('My First Test', function() {
    it('TRUE!', function() {
      expect(true).to.equal(true)
    })
    it('FALSE!', function() {
        expect(false).to.equal(false)
    })
})

describe('Test Game Page', () => {
    //before each test, load the game page
    beforeEach(() => {
        cy.visit('http://localhost:3000')    
    })

    // Firstly, make sure page renders as expected
    describe('Renders Page Correctly', function() {
        it('Finds the game header', () => {
            cy.contains('Game 1')
            cy.get('h4').should('contain', 'Game 1')
        })
        it('Finds the game', () => {
            cy.get('div').should('have.class', 'game')
        })
        it('Finds the game board', () => {
            cy.get('div').should('have.class', 'game-board')
        })
        it('Finds the row', () => {
            cy.get('.board-row').should('have.class', 'board-row').then(($div) => {
                expect($div).to.have.length(3);
            })
        })
        it('Finds the squares', () => {
            cy.get('.square').should('have.class', 'square').then(($divs) => {
                expect($divs).to.have.length(9);
                expect($divs).to.contain('');
            })
        })
        it('Finds the game-info', () => {
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Next player: X');
            });
        })
        it('Finds the game-status', () => {
            cy.get('.game-status').should('have.class', 'game-status');
        })
        it('Finds the game-history', () => {
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(1);
                expect($li.eq(0)).to.contain('Go to game start');
            })
        })
    })

    describe('First move', () => {
        it('should perform first move', () => {
            cy.get('.square').eq(0).click();
            cy.get('.square').eq(0).should((butt) => {
                expect(butt).to.contain('X');
            })
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(2);
                expect($li.eq(0)).to.contain('Go to game start');
                expect($li.eq(1)).to.contain('Go to move #1');
            })
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Next player: O');
            });
        }) 
    })

    describe('Second move', () => {
        it('should perform second move', () => {
            cy.get('.square').eq(0).click();            
            cy.get('.square').eq(1).click();
            cy.get('.square').eq(1).should((butt) => {
                expect(butt).to.contain('O');
            })
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(3);
                expect($li.eq(0)).to.contain('Go to game start');
                expect($li.eq(1)).to.contain('Go to move #1');
                expect($li.eq(2)).to.contain('Go to move #2');
            })
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Next player: X');
            });
        }) 
    })

    describe('Third move', () => {
        it('should perform third move', () => {
            cy.get('.square').eq(0).click();            
            cy.get('.square').eq(1).click();
            cy.get('.square').eq(4).click();
            cy.get('.square').eq(4).should((butt) => {
                expect(butt).to.contain('X');
            })
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(4);
                expect($li.eq(0)).to.contain('Go to game start');
                expect($li.eq(1)).to.contain('Go to move #1');
                expect($li.eq(2)).to.contain('Go to move #2');
                expect($li.eq(3)).to.contain('Go to move #3');
                /* UNCOMMENT BELOW FOR ERROR DEMO */
                // expect($li.eq(2)).to.contain('Go to move #3');
            })
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Next player: O');
            });
        }) 
    })

    describe('Fourth move', () => {
        it('should perform fourth move', () => {
            cy.get('.square').eq(0).click();            
            cy.get('.square').eq(1).click();
            cy.get('.square').eq(4).click();
            cy.get('.square').eq(5).click();
            cy.get('.square').eq(5).should((butt) => {
                expect(butt).to.contain('O');
            })
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(5);
                expect($li.eq(0)).to.contain('Go to game start');
                expect($li.eq(1)).to.contain('Go to move #1');
                expect($li.eq(2)).to.contain('Go to move #2');
                expect($li.eq(3)).to.contain('Go to move #3');
                expect($li.eq(4)).to.contain('Go to move #4');
            })
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Next player: X');
            });
        }) 
    })

    describe('Final move', () => {
        it('should perform final move', () => {
            cy.get('.square').eq(0).click();            
            cy.get('.square').eq(1).click();
            cy.get('.square').eq(4).click();
            cy.get('.square').eq(5).click();
            cy.get('.square').eq(8).click();
            cy.get('.square').eq(8).should((butt) => {
                expect(butt).to.contain('X');
            })
            cy.get('.game-history li').then(($li) => {
                expect($li).to.have.length(6);
                expect($li.eq(0)).to.contain('Go to game start');
                expect($li.eq(1)).to.contain('Go to move #1');
                expect($li.eq(2)).to.contain('Go to move #2');
                expect($li.eq(3)).to.contain('Go to move #3');
                expect($li.eq(4)).to.contain('Go to move #4');
                expect($li.eq(5)).to.contain('Go to move #5');
            })
            cy.get('.game-info').then(($div) => {
                expect($div).to.contain('Winner: X');
            });
        }) 
    })
})
