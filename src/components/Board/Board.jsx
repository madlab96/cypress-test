import React, { Component } from 'react';
import '../../index.css';
import {Square} from '../Square/Square'        

export class Board extends Component {
    constructor(props) {
        super(props);
        this.renderSquare = this.renderSquare.bind(this);
    }

    renderSquare(i) {
        return (
            <Square
                key={i.toString()}
                value={this.props.squares[i]}
                onClick={() => this.props.onClick(i)}
            />
        );
    }
  
    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}