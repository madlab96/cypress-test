import React from 'react';
import ReactDOM from 'react-dom';
import { Square } from './Square';

import {shallow, mount, render, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("Square Tests", () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Square />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
    it('Enzyme tests square exists', function() {
        expect(shallow(<Square />).contains(<button className="square"></button>)).toBe(true);
    });
})